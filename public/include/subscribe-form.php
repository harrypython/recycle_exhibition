<?php

include('./MailChimp.php'); 
use \DrewM\MailChimp\MailChimp;

// Step 1 - Enter your Mailchimp API KEY - more info: http://kb.mailchimp.com/article/where-can-i-find-my-api-key
$apiKey 	= '9ef5c493376cc6062eacf986110a4794-us12';

// Step 2 - Enter your Mailchimp ListId code - more info: http://kb.mailchimp.com/article/how-can-i-find-my-list-id
$listId 	= 'bc822b69ef';

$email = $_POST['widget-subscribe-form-email'];
$firstname = isset( $_POST['widget-subscribe-form-firstname'] ) ? $_POST['widget-subscribe-form-firstname'] : '';
$lastname = isset( $_POST['widget-subscribe-form-lastname'] ) ? $_POST['widget-subscribe-form-lastname'] : '';

$MailChimp = new MailChimp($apiKey);

if(isset($email) AND $email != '') {

    $result = $MailChimp->post("lists/$listId/members", [
        'email_address' => $email,
        'status'        => 'subscribed',
    ]);
    
    if ($MailChimp->success()) {
        $arrResult = array ('response'=>'success');
    }else{
        $arrResult = array ('response'=>'error','message'=>$MailChimp->getLastError());
    }

    echo json_encode($arrResult);
}

?>
