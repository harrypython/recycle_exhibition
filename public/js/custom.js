function checkDateTime() {
    // Get the current date and time in GMT-3 (Brasilia time)
    var now = new Date();
    now.setHours(now.getHours() - 3); // Adjust to GMT-3
    
    // Set the target date and time (2023/07/13 20:30:00 GMT-3)
    var targetDate = new Date('2023-07-13T20:30:00');
    
    // Compare the current date and time with the target date and time
    if (now > targetDate) {
      // If the current date and time is after the target date and time, hide the elements
      $('.before-opening').hide();
      $('.after-opening').show();
      return true;
    } else {
      // If the current date and time is before or equal to the target date and time, do nothing
      return false;
    }
  }

  function getArtists() {
    $.getJSON('include/members.json', function(data) {
      // Loop through the artists data
      $.each(data, function(index, artist) {


        // Create the team member container
        var teamMember = $('<div class="team-member"></div>');
  
        // Create the team image container and append the image
        var teamImage = $('<div class="team-image"></div>');
        teamImage.append($('<img>', { src: artist.image }));
  
        // Create the team description container and append the artist details
        var teamDesc = $('<div class="team-desc"></div>');
        teamDesc.append($('<h3>').text(artist.name));
        teamDesc.append($('<span>').text(artist.title));
        teamDesc.append($('<p>').text(artist.desc));
  
        // Create the social links container
        var socialLinks = $('<div class="align-center"></div>');
  
        // Create the Twitter link if the URL is provided
        if (artist.twitter) {
          var twitterLink = $('<a>', {
            class: 'btn btn-xs btn-slide btn-light',
            href: artist.twitter,
            target: '_blank',
            'data-width': '100'
          });
          twitterLink.append($('<i class="fab fa-twitter"></i>'));
          twitterLink.append($('<span>').text('Twitter'));
          socialLinks.append(twitterLink);
        }
  
        // Create the Instagram link if the URL is provided
        if (artist.instagram) {
          var instagramLink = $('<a>', {
            class: 'btn btn-xs btn-slide btn-light',
            href: artist.instagram,
            target: '_blank',
            'data-width': '118'
          });
          instagramLink.append($('<i class="fab fa-instagram"></i>'));
          instagramLink.append($('<span>').text('Instagram'));
          socialLinks.append(instagramLink);
        }
  
        // Create the Website link if the URL is provided
        if (artist.website) {
          var websiteLink = $('<a>', {
            class: 'btn btn-xs btn-slide btn-light',
            href: artist.website,
            target: '_blank',
            'data-width': '80'
          });
          websiteLink.append($('<i class="fa fa-globe"></i>'));
          websiteLink.append($('<span>').text('Website'));
          socialLinks.append(websiteLink);
        }
  
        // Append the team image, description, and social links to the team member container
        teamMember.append(teamImage);
        teamMember.append(teamDesc);
        teamDesc.append(socialLinks);
  
        // Append the team member to the container in the HTML page
        
        var teamMemberRow = $('<div class="col-lg-3" data-animate="fadeInUp"></div>')
        teamMemberRow.append(teamMember);
        $('#artists-container').append(teamMemberRow);
      });
    });
  }
  
  
  // Call the function when the page finishes loading
  $(document).ready(function() {
    // checkDateTime();
    // getArtists();
  });
  